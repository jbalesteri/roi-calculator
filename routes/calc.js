const express = require('express');
const router = express.Router();
const {authorize, google} = require('../config');
const fs = require('fs');
const debug = require('debug')('route: calc')
const spreadsheetId = "1mULA0MgUV_HmkriNw5ym4uQ45Vj7jHiXfFkb19QuWQE";

outputCtrl = (req, res, next) => {
  fs.readFile('client_secret.json', (err, content) => {
    if (err) return debug('Error loading client secret file:', err);
    const range = "CUSTOMER!C19:C23";
    authorize(JSON.parse(content), (auth) => {
      const sheets = google.sheets({ version: 'v4', auth });
      return sheets.spreadsheets.values.get({
        spreadsheetId, range
      }, (err, result) => {
        if (err) {
          debug(err);
        } else {
          debug("Success!");
          req.dataProcessed = {
            title: 'ROI Calculator',
            year: (new Date()).getFullYear(),
            calculations: result.data.values
          };
        }
        return next();
      });
    });
  });
}

renderCtrl = (req, res, next) => {
  var context = {
    ...req.dataProcessed,
    labels: [
      "One time hardware investment",
      "Annual Software investment",
      "ROI Value in $ (10yrs)",
      "ROI in Percent (10yrs)",
      "Payback Period"
    ]};
  res.render('calc', context);
  return next();
}

fileCopyCtrl = (req, res) => {
  debug('Copying file on drive...');
  fs.readFile('client_secret.json', (err, content) => {
    if (err) return debug('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const drive = google.drive({version: 'v3', auth});
      const body = {
        'name': req.query.fileName,
        'parents': ["1AcV5RSkqIOXNV6xUAKjvaDPnspgW9YMd"]
      }
      return drive.files.copy({
        'fileId': spreadsheetId,
        'resource': body
      }, (err, result) => {
        if (err) {
          debug(err);
        } else {
          debug("Copy Success!");
          // return next(); 
        }
      });
    });
  });
}

router.get('/', outputCtrl, renderCtrl, fileCopyCtrl);

module.exports = router;
