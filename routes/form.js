var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator/check');
const {authorize, google} = require('../config');
const fs = require('fs');
const debug = require('debug')('route: form')

const spreadsheetId = "1mULA0MgUV_HmkriNw5ym4uQ45Vj7jHiXfFkb19QuWQE";
const inputFields = [
  {
    label: 'Name',
    name: 'name',
    type: 'text',
    required: true,
    default: ''
  },
  {
    label: 'Title',
    name: 'title',
    type: 'text',
    default: ''
  },
  {
    label: 'Company',
    name: 'company',
    type: 'text',
    default: ''
  },
  {
    label: 'City',
    name: 'city',
    type: 'text',
    default: ''
  },
  {
    label: 'State',
    name: 'state',
    type: 'text',
    default: ''
  },
  {
    label: 'Email Address',
    name: 'email',
    type: 'text',
    default: ''
  },
  { // Utility Power in MW (Min: 0.5 Max: 50; Type: Decimal; Max one decimal place;)
    label: 'Utility Power (MW) ',
    helperText: '(0.5 - 50)',
    name: 'utilPower',
    type: 'text',
    required: true,
    default: ''
  },
  { // UPS Max Normal Loading in Percent (Min: 20; Max 100; Type: Integer)
    label: 'UPS Max Normal Loading (%) ',
    helperText: '(20 - 100)',
    name: 'upsMax',
    type: 'number',
    required: true,
    default: ''
  },
  { // Rack Allocated Power in kW (Min 1, Max 50, Type: Integer)
    label: 'Rack Allocated Power (kW)',
    helperText: '(1 - 50)',
    name: 'rackAllocPwr',
    type: 'number',
    required: true,
    default: ''
  },
  { // Rack Normal Utilization in Percent (Min: 10, Max 100; Type: Integer)
    label: 'Rack Normal Utilization (%) ',
    helperText: '(Observed 10 - 100)',
    name: 'rackUtil',
    type: 'number',
    required: true,
    default: ''
  },
  { // Capex Cost per MW in $Million (Min: 1, Max: 20; Type: Integer; Optional - Defaults to 8)
    label: 'Capex Cost per MW ($1M)',
    helperText: '(1 - 20)',
    name: 'capExCost',
    type: 'number',
    required: true,
    default: 8
  },
  { // Opex Cost per MW in $Million (Min: 1, Max 5; Type: Integer; Optional - Defaults to 2)
    label: 'Opex Cost per MW ($1M) ',
    helperText: '(1 - 5)',
    name: 'opExCost',
    type: 'number',
    required: true,
    default: 2
  }
];

/* GET home page. */
router.get('/', function(req, res, next) {
  debug('sess:: ', req.session);
  req.session.data = inputFields;
  res.render('index', {
    title: 'ROI Calculator',
    year: (new Date()).getFullYear(),
    fields: req.session.data,
    errors: req.session.errors || []
  });
});

inputCtrl = (req, res, next) => {
  const range = "CUSTOMER!C3:C14";
  const values = [
    [req.body.name],
    [req.body.title],
    [req.body.company],
    [req.body.city],
    [req.body.state],
    [req.body.email],
    [req.body.utilPower],
    [`${req.body.upsMax}%`],
    [req.body.rackAllocPwr],
    [`${req.body.rackUtil}%`],
    [req.body.capExCost],
    [req.body.opExCost]
  ]

  fs.readFile('client_secret.json', (err, content) => {
    if (err) return debug('Error loading client secret file:', err);
    authorize(JSON.parse(content), (auth) => {
      const sheets = google.sheets({ version: 'v4', auth });
      const valueInputOption = 'USER_ENTERED';
      const resource = {values};
      sheets.spreadsheets.values.update({
        spreadsheetId, range, valueInputOption, resource
      }, (err, result) => {
        if (err) {
          debug(err);
        } else {
          debug("Success!");
          return res.redirect('/calc?fileName=' + encodeURIComponent(req.body.name + '-' + new Date ));
        }
      });
    });
  });
}

validationCtrl = (req, res, next) => {
  req.session.errors = validationResult(req);
  // debug('req.session.errors: ', req.session.errors.array());
  req.session.data = inputFields;
  debug('validationCtrl > req.session.data: ', req.session.data);
  if (req.session.errors) {
    req.session.data.map(field => {
      if(req.body[field.name] !== ''){
        field.value = req.body[field.name];
      }
    });

    let context = {
      title: 'ROI Calculator',
      year: (new Date()).getFullYear(),
      fields: req.session.data,
      errors: req.session.errors.array()
    }
    debug('context: ', context);
    // debug('session: ', req.session);

    res.render('index', context);
  } else {
    return next();
  }
}

validations = [
  check('name').isLength({ min: 4 }).withMessage('Name must be 4 chars'),
  check('utilPower').isFloat({gt: 0.49, max: 50.0 }).withMessage('Must be between 0.5 and 50'),
  check('upsMax').isInt({ min: 20, max: 100}).withMessage('Must be between 20 and 100'),
  check('rackAllocPwr').isInt({ min: 1, max: 50}).withMessage('Must be between 1-50'),
  check('rackUtil').isInt({ min: 10, max: 100}).withMessage('Must be between 10-100'),
  check('capExCost').isInt({ min: 1, max: 20}).withMessage('Must be between 1-20'),
  check('opExCost').isInt({ min: 1, max: 5}).withMessage('Must be between 1-5')
]

router.post('/', validations, validationCtrl, inputCtrl);

module.exports = router;
