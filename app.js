const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const uuid = require('uuid');
const app = express();

const formRouter = require('./routes/form');
const calcRouter = require('./routes/calc');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
var sess = {
  secret: 'vps roi calculation',
  genid: function(req) {
    return uuid() // use UUIDs for session IDs
  },
  cookie: {
    maxAge: 60000,
    secure: false
  },
  resave: false,
  saveUninitialized: true,
  errors: [],
  data: []
}

if (app.get('env') === 'production') {
  app.set('trust proxy', 1);
}

app.use(session(sess));
app.use('/', formRouter);
app.use('/form', formRouter);
app.use('/calc', calcRouter);

// 404
app.use(function(req, res, next) {
  next(createError(404));
});
// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
