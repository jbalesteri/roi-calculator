# roi-calculator
ROI Calculator for VPS hardware

## Running app
Download or clone repo
```npm install && npm start```

*To run with debuggers*
```npm run dev```

## Scope of project
1. Generate basic Express app w/ templating
2. Break down layout:
    - Partials for layout
        - Header
        - Footer
        - Form
        - Calculations
3. Compartmentalize functionality:
    - Form Validation
        - Necessary fields have data
        - Proper ranges are adhered to
            - **?:** Are there ranges?
    - Google sheets API interaction
        - Copy ROI calc sheet(s)
        - Input appropriate data
            - **?:** should there be a "top sheet" that only has input fields in it?
        - Saves completed sheet for internal use
            - Establish methodology/naming convention
            - Decide on multiple saves per user or overwrite existing?
        - Returns calculation values
4. Styling
5. Testing:
    - **?:** Necessary?
    - **?:** Type of testing? (unit, e2e)
6. Deployment:
    - Deploy to GE
    - VPS corporate site (via iFrame)
    - **?:** Utilized anywhere else?
7. Consider enhancements:
    - Data Visualization
        - Charts
        - Graphs